# Selecting a Funeral Home

Selecting the right funeral home to handle a loved one's funeral (or help pre-plan your own) needn't be a stressful process. It is, nevertheless, important to be aware of three mistakes to be avoided when making your choice. Keep these points in mind as you start your search and your dealings with the funeral home should be more than sufficient.

Mistake #1: The Most Expensive Funeral Home is Best

Although the costs for funeral-related services can be abundant, the cost isn't always an indicator of qualityfor your needs such as a [green burial](https://www.fernwood.com/green-cemetery-san-francisco-bay-area
). Even between funeral homes in the corresponding area, you may find a broad range of costs for the same types of services rendered.

Don't be afraid to ask for a breakdown of costs when services are presented in a package. If a singular service seems costly, ask if there is a less expensive option. Unless you find a substantial reason for the cost of services to be more when provided by a specific funeral home, opt for the one you can most conveniently afford.

Mistake #2: All Funeral Homes are the Same

As with Mistake #1, it's straightforward to make assumptions. After all, how many times in our lives do we find ourselves planning a funeral? It may seem that they all perform the same basic services in about the same manner, but that's not the case.

One primary difference between funeral homes is how they provide services. Are they attentive to customers' preferences, or do they steer them to the same fundamental options? Are they respectful of your budget or financial limitations? Are the funeral director and staff courteous and accommodating? Are the facilities well-maintained and the products offered of good quality? By paying consideration to the way you are treated and the overall quality of the facility, you can select a firm that will best help you plan the funeral service you desire.

Mistake #3: My Options Are Limited When Selecting Funeral-Related Services

When working with a professional funeral home staff, you should be able to shape the funeral or memorial service you're intending to best honor the deceased. While there may be laws or ordinances that require certain funeral elements (for example, a cemetery may require the purchase of a vault, or embalming before burial) if you feel you're being limited to what the funeral director prefers, ask for more options or work with another firm.

Planning a funeral that reflects the life of the person who has died includes adding personal elements like favorite songs, an appropriate poem or scripture, or specially printed funeral programs and memorial keepsakes. Don't settle for a "generic" service; pick one that will work with you to personalize this unique event.

Picking the right funeral home isn't difficult if you keep in mind that the most expensive isn't undoubtedly best, not all are the same and you should have options when selecting funeral-related services. A funeral home that reflects these three values will never be a mistake to pick from.